//
//  FeedItem.swift
//  FlickrFeed
//
//  Created by Hayk Musheghyan on 6/3/17.
//  Copyright © 2017 Tigerspike. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class FeedItem{
    
    var title : String = ""
    var link : String = ""
    var imageURL : String = ""
    var dateTaken : String = ""
    var published : String = ""
    var author : String = ""
    var tag : String = ""
    var authorId : String = ""
    
    var publishedDATE : Date?
    var takenDATE : Date?
    
    init(withJSON source: JSON) {
        
        self.title = source["title"].stringValue
        self.link = source["link"].stringValue
        self.imageURL = source["media"]["m"].stringValue
        self.dateTaken = self.convertDateFormat(date: source["date_taken"].stringValue.components(separatedBy: "T")[0])
        self.published = self.convertDateFormat(date: source["published"].stringValue.components(separatedBy: "T")[0])
        self.author = source["author"].stringValue
        self.tag = source["tags"].stringValue
        self.authorId = source["author_id"].stringValue
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.takenDATE = self.convertStringToDate(date: self.dateTaken)
        self.publishedDATE = self.convertStringToDate(date: self.published)
        
        
        
    }
    
    func convertDateFormat(date:String) ->String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        let dateObj = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        return dateFormatter.string(from: dateObj!)
    }
    
    func convertStringToDate (date:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.date(from: date)!
    }
}
