//
//  ViewController.swift
//  FlickrFeed
//
//  Created by Hayk Musheghyan on 6/3/17.
//  Copyright © 2017 Tigerspike. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation
import Haneke
import MessageUI

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, MFMailComposeViewControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var spinner: UIActivityIndicatorView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var sortButton: UIButton!
    @IBOutlet var pickerView: UIPickerView!
    
    private let refreshControl = UIRefreshControl()
    var imagePosts = [FeedItem]()
    var imagePostsLocalCachedCopy = [FeedItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        self.searchTextField.delegate = self

        self.tableView.register(UINib(nibName: "FeedTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedTableViewCell")
        refreshControl.addTarget(self, action: #selector(loadImages), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
       
        self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.onDrag
        self.tableView.isHidden = true
        self.loadImages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //api request for getting images
    func loadImages() {
        
        self.spinner.startAnimating()
        self.spinner.isHidden = false
        self.imagePosts.removeAll()
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        let url = URL(string: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            if error != nil {
                
                self.showError(message: error!.localizedDescription)
                print(error!.localizedDescription)
                
            } else {
                
                if data != nil {
                    let cache = Shared.dataCache
                    cache.set(value: data!, key: "postArray")
                }
                
                do {
                
                    let json = JSON(data!)
                    
                    var postsArray = [FeedItem]()
                    if let array = json["items"].array {
                        for post in array {
                            let postItem = FeedItem(withJSON: post)
                            
                            postsArray.append(postItem)
                        }
                        
                        self.imagePostsLocalCachedCopy = postsArray
                        self.imagePosts = postsArray
                        self.reload()
                        
                        self.tableView.isHidden = false

                    } else {
                        self.showError(message: "Please try again later")
                        self.loadImages()
                    }
                    
                    self.spinner.stopAnimating()
                    self.spinner.isHidden = true
                    
                }
            }
        })
        task.resume()
        self.refreshControl.endRefreshing()
    }
    //search handle
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        
        var sortedArray = [FeedItem]()
        
        if txtAfterUpdate == "" {
            self.imagePosts = self.imagePostsLocalCachedCopy
            self.reload()
        } else {
            
            for post in self.imagePostsLocalCachedCopy {
                
                if post.tag != "" {
                    if post.tag.lowercased().range(of:txtAfterUpdate) != nil{
                        sortedArray.append(post)
                    }
                }
                
            }
            self.imagePosts.removeAll()
            self.imagePosts = sortedArray
            self.reload()
        }
        
        return true
    }

    //tableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.imagePosts.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 450
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell
        
        if self.imagePosts.count > 0 {
            cell.populate(withFeedItem: self.imagePosts[(indexPath as NSIndexPath).row])
        }
        
        //load more can be done also if 10 left to reach the end of the list
        if (indexPath as NSIndexPath).row + 10 > self.imagePosts.count {
//            self.loadImages()
            //and a bit of editing needed fot the image array and will work
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.openOptions(indexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    //pickerView methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "Published Date"
        } else {
            return "Taken Date"
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if row == 0 {
            self.sortBy(category: "Published")
            self.sortButton.setTitle("Published", for: UIControlState.normal)
        } else {
            self.sortBy(category: "Taken")
            self.sortButton.setTitle("Taken", for: UIControlState.normal)
            
        }
        
        self.pickerView.isHidden = true
    }

    @IBAction func onSortButtonClicked(_ sender: Any) {
        self.pickerView.isHidden = false
    }
    
    func sortBy(category: String) {
        
        if category == "Published" {
            self.imagePosts = self.imagePostsLocalCachedCopy
            self.reload()
        } else {
            self.imagePosts.sort(by: { $0.takenDATE?.compare(($1.takenDATE)!) == .orderedAscending})
            self.reload()
        }
    }
    
    //options to choose from tableview cell click
    func openOptions(indexPath : IndexPath) {
        
        let alert = UIAlertController(title:nil , message:nil, preferredStyle: UIAlertControllerStyle.actionSheet)
  
        alert.addAction(UIAlertAction(title: "Save Photo", style: UIAlertActionStyle.default, handler: { action in
                
                UIImageWriteToSavedPhotosAlbum(self.getSavedImage(withFeedItem: self.imagePosts[(indexPath as NSIndexPath).row]), self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
   
            }))
        
        alert.addAction(UIAlertAction(title: "Open in Safari", style: UIAlertActionStyle.default, handler: { action in
            let imageUrl = NSURL(string: self.imagePosts[(indexPath as NSIndexPath).row].imageURL)! as URL
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(imageUrl, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(imageUrl)
            }
   
        }))
        
        alert.addAction(UIAlertAction(title: "Email Photo", style: UIAlertActionStyle.default, handler: { action in
            
            self.postEmail(image: self.getSavedImage(withFeedItem: self.imagePosts[(indexPath as NSIndexPath).row]))
        }))
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: { action in }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func postEmail(image: UIImage) {
        let mail:MFMailComposeViewController = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setSubject("Attached photo")
        
        let image = image
        let imageString = returnEmailStringBase64EncodedImage(image: image)
        let emailBody = "<img src='data:image/png;base64,\(imageString)' width='\(image.size.width)' height='\(image.size.height)'>"
        
        mail.setMessageBody(emailBody, isHTML:true)
        
        self.present(mail, animated: true, completion:nil)
    }
    
    //encode pic for email use
    func returnEmailStringBase64EncodedImage(image:UIImage) -> String {
        let imgData:NSData = UIImagePNGRepresentation(image)! as NSData;
        let dataString = imgData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return dataString
    }

    //message saying if saved or no
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    //error messages
    func showError(message:String) {
        
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }

    func reload() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    //Can download the pic again or this could be done also from already chached ones
    func getSavedImage(withFeedItem post: FeedItem) -> UIImage {
        let cache = Shared.imageCache
        var cachecdImage : UIImage?
        
        //getting the image from cache by title(caching by title at this case is not right but i used it as the app is really small)
        cache.fetch(key: "\(post.title)").onSuccess { (image) in
            
            cachecdImage = image
            
            }.onFailure { (error) in
                // download image again 
                //or some error message handled 
            }

        return cachecdImage!
        
    }

}

