//
//  FeedTableViewCell.swift
//  FlickrFeed
//
//  Created by Hayk Musheghyan on 6/3/17.
//  Copyright © 2017 Tigerspike. All rights reserved.
//

import Foundation
import UIKit
import Haneke

class FeedTableViewCell : UITableViewCell {
    
    @IBOutlet var postImageView: UIImageView!
    @IBOutlet var postTitle: UILabel!
    @IBOutlet var publishedTime: UILabel!
    @IBOutlet var otherMetadata: UILabel!

    func populate(withFeedItem post: FeedItem) {
        
        self.postTitle.text = post.title
        self.publishedTime.text = post.published
        // date could be also done with timestamps, elapsed time etc...
        
        let index = post.author.index(post.author.startIndex, offsetBy: 20)
        var autorUsername = post.author.substring(from: index)
        let endIndex = autorUsername.index(autorUsername.endIndex, offsetBy: -2)
        autorUsername = autorUsername.substring(to: endIndex)
        self.otherMetadata.text = "User: " + autorUsername + "\n" + "Date taken: " + post.dateTaken

        setImage(withFeedItem: post)
        self.layoutIfNeeded()
        
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.postTitle.text = ""
        self.postImageView.image = nil
        self.publishedTime.text = ""
        self.otherMetadata.text = ""

    }
    
    func setImage (withFeedItem post: FeedItem) {
        
        let cache = Shared.imageCache
        
        cache.fetch(key: "\(post.title)").onSuccess { (image) in
            
            self.postImageView.image = image
            
            }.onFailure { (error) in
                
                let url = URL(string: post.imageURL)
                
                DispatchQueue.global().async {
                    if let data = try? Data(contentsOf: url!) {
                        
                        if let postImage = UIImage(data: data) {
                            
                            cache.set(value: postImage, key: "\(post.title)")
                            DispatchQueue.main.async {
                                self.postImageView.image = UIImage(data: data)
                            }
                        } else {
                            DispatchQueue.main.async {
                                // assuming a placeholder will be set
                            }
                        }
                    }
                }
        }
    }
}
